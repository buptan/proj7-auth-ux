<html>
  <head>
    <title>ListAll</title>
  </head>
  <body>
    <h1>List All</h1>
    <ul>
      <?php
      $json = file_get_contents('http://laptop-service/listAll');
      $obj = json_decode($json);
      $controls = $obj->control_points;
      foreach($controls as $c){
        echo "<li>$c</li>";
      }
      ?>
    </ul>
    <input type="submit" class="button" name="back" value="back" onclick="location.href='/'" />
  </body>
</html>
