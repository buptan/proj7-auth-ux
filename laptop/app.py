import os
import flask
from flask import redirect, url_for, request, render_template, jsonify, flash, abort
from flask_restful import Resource, Api
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from wtforms import Form, BooleanField, StringField, validators, TextField
from flask_wtf import FlaskForm
import arrow
from pymongo import MongoClient
from passlib.apps import custom_app_context as pwd_context
import acp_times
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature,
                            SignatureExpired)

app = flask.Flask(__name__)
api = Api(app)

client = MongoClient('172.32.2.3', 27017)
db = client.tododb
edb = client.entries
udb = client.users

SECRET_KEY = "yada yada yada"

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active
        self.token = None

    def is_active(self):
        return self.active


app.config.from_object(__name__)
#step 1 in slides
login_manager = LoginManager()
#step 6 in slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page"
login_manager.refresh_view = "reauth"
login_manager.setup_app(app)

def generate_auth_tok(user_id, expiration=600):
    s = Serializer('testing123haha', expires_in=expiration)
    return s.dumps({'uid': user_id})

def verify_auth_tok(token):
    s = Serializer('testing123haha')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None
    except BadSignature:
        return None
    return "Success"

#step 2 in slides
@login_manager.user_loader
def load_user(user_id):
    _users = udb.entries.find()
    users = [user for user in _users]
    for user in users:
        if int(user_id) == user["uid"]:
            USER = User(user["username"], user["uid"])
            return USER
    return None

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell',
            'Windozzee',
            'Yet another laptop!',
            'Yet yet another laptop!',
            'Test']
        }

api.add_resource(Laptop, '/example')

class ListAllJSON(Resource):
    def get(self):
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        data = [0 for i in range(j)]
        i = 0
        for entry in entries:
            data[i] = "km: {}   open_time: {}   close_time: {}".format(entry['km'], entry['open_time'], entry['close_time'])
            i = i + 1

        return { 'control_points': data }

api.add_resource(ListAllJSON, '/listAll', '/listAll/json')

class ListOpenJSON(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            data[i] = "km: {}   open_time: {}".format(entry['km'], entry['open_time'])
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListOpenJSON, '/listOpenOnly/', '/listOpenOnly/json')

class ListCloseJSON(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            data[i] = "km: {}   close_time: {}".format(entry['km'], entry['close_time'])
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListCloseJSON, '/listCloseOnly', '/listCloseOnly/json')

class ListAllCSV(Resource):
    def get(self):
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        data = [0 for i in range(j)]
        i = 0
        for entry in entries:
            item = "{}, {}, {}".format(entry['km'], entry['open_time'], entry['close_time'])
            data[i] = item
            i = i + 1

        return { 'control_points': data }

api.add_resource(ListAllCSV, '/listAll/csv')

class ListOpenCSV(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            item = "{}, {}".format(entry['km'], entry['open_time'])
            data[i] = item
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListOpenCSV, '/listOpenOnly/csv')

class ListCloseCSV(Resource):
    def get(self):
        if 'top' in request.args:
            top = int(request.args['top'])
        else:
            top = 0
        _entries = edb.entries.find()
        entries = [entry for entry in _entries]
        j = 0
        for entry in entries:
            j += 1
        if top == 0:
            data = [0 for i in range(j)]
        else:
            data = [0 for i in range(top)]
        i = 0
        for entry in entries:
            item = "{}, {}".format(entry['km'], entry['close_time'])
            data[i] = item
            i = i + 1
            if top != 0:
                if top == i:
                    break

        return { 'control_points': data }

api.add_resource(ListCloseCSV, '/listCloseOnly/csv')

class RegistrationForm(FlaskForm):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    password = StringField('Password', [validators.Length(min=6, max=30)])
    status = StringField('Status')

#TODO: Add errorchecking: ex--username already registered. DONE--need to test
@app.route("/api/register", methods=("POST", "GET"))
def register():
    form = RegistrationForm(request.form)
    if request.method=="POST" and form.validate():
        register = 1
        uid = 0
        _users = udb.entries.find()
        users = [user for user in _users]
        for user in users:
            app.logger.debug(user['uid'])
            if user['uid'] == uid:
                uid = uid + 1
            if form.username.data == user['username']:
                register = 0
        if register:
            user_entry = {
                    "uid": uid,
                    "username": form.username.data,
                    "password": pwd_context.encrypt(form.password.data)
            }
            udb.entries.insert_one(user_entry)
            app.logger.debug("Inserted User")
            form.status.data = "Success!"
            return render_template("register.html", form=form)
        else:
            return render_template("400.html"), 400
    elif request.method=="GET":
        return render_template("register.html", form=form)
    else:
        return render_template("400.html"), 400

#step 3 in slides
#TODO
@app.route("/api/login", methods=["GET", "POST"])
def login():
    form = RegistrationForm(request.form)
    if request.method == "POST" and form.validate():
        _users = udb.entries.find()
        users = [user for user in _users]
        count = len(users)
        USERS = []
        for i in range(count):
            USERS.append(User(users[i]["username"], users[i]["uid"]))
        for user in USERS:
            if user.name == form.username.data:
                if login_user(user):
                    #logged in successfully
                    return redirect(url_for('index'))

    elif request.method == "GET":
        return render_template("login.html", form=form)

@app.route("/api/token", methods=["GET", "POST"])
@login_required
def get_user_token():
    t = generate_auth_tok(current_user.get_id())
    current_user.token = t
    app.logger.debug(current_user.token)
    return redirect("/")

@app.route('/_view_udb')
def view_udb():
    app.logger.debug(current_user.token)
    try:
        if verify_auth_tok(current_user.token) != None:
            _entries = udb.entries.find()
            entries = [entry for entry in _entries]

            return render_template('view_udb.html', entries=entries)
        return abort(401)
    except:
        return abort(401)

@app.route('/display')
def display():
    _entries = edb.entries.find()
    entries = [entry for entry in _entries]

    return render_template('display.html', entries=entries)

@app.route('/clear_udb')
def clear_udb():
    udb.entries.remove({})
    return render_template('clear_edb.html')

@app.route('/clear_edb')
def clear_edb():
    edb.entries.remove({})
    return render_template('clear_edb.html')

@app.route("/")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open and close times from km,
    using rules described at https://rusa.org/octime_alg.html.
    Expects 4 URL-encoded arguments, the number of km,
    the brevet category, the begin date and begin time.
    """
    app.logger.debug("got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_cat = request.args.get('brevet_dist', type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')

    #app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    add = [0,0,0]
    sect = 0
    ct = 0
    for char in begin_date:
        if char == '-':
            if ct == 1:
                add[sect] = 1
            ct = 0
            sect = sect + 1
        else:
            ct = ct + 1
    d_string = ""
    sect = 0
    for char in begin_date:
        if char ==  '-':
            sect = sect + 1
        elif add[sect] == 1:
            d_string = d_string + '0'
        d_string = d_string + char

    use_time = arrow.get(d_string + ' ' + begin_time, 'DD-MM-YYYY HH:mm')
    use_time.replace(tzinfo='US/Pacific')

    open_time = acp_times.open_time(km, brevet_cat, use_time)
    close_time = acp_times.close_time(km, brevet_cat, use_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/_handle_submit', methods=['POST','GET'])
def _handle_submit():
    data = request.get_json()
    app.logger.debug("handling submit button press")
    app.logger.debug(data)
    for entry in data:
        if entry[1] != "":
            entry_doc = {
                'km': entry[1],
                'open_time': entry[3],
                'close_time': entry[4]
            }
            #app.logger.debug(entry_doc)
            edb.entries.insert_one(entry_doc)
    result = ""
    return result


@app.route('/new')
def new():
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
